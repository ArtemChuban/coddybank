from flask import Flask, render_template, request
import datetime
import json

app = Flask(__name__)


def check_code(code: str) -> bool:
    return code == f"{datetime.datetime.now(datetime.timezone(datetime.timedelta(hours=3))).hour}{datetime.datetime.now().minute}"


class Bank:
    __accounts = {x: 0 for x in [
        'Андрей Гагарин',
        'Вера Гурская',
        'Георгий Павлычев',
        'Глеб Громов',
        'Григорий Бочкарев',
        'Дарья Смирнова',
        'Денис Клопов',
        'Ева Исмоилова',
        'Егор Сидельников',
        'Иван Страшников',
        'Марк Кисин',
        'Марта Залысина',
        'Матвей Виссинг',
        'Милана Шпакова',
        'Петр Новоселов',
        'Растислав Евтушенко',
        'Савелий Володин',
        'Тимофей Фролов',
        'Тимур Кышлалы',
        'Ульяна Суднишникова',
        'Федор Шкляренко'
    ]}

    def __init__(self) -> None:
        try:
            self.load()
        except FileNotFoundError:
            self.save()

    def save(self) -> None:
        with open("/mnt/db.json", "w") as db:
            json.dump(self.__accounts, db)

    def load(self) -> None:
        with open("/mnt/db.json", "r") as db:
            self.__accounts = json.load(db)

    @property
    def accounts(self) -> dict[str, int]:
        return self.__accounts

    def add_transaction(self, name: str, value: int, description: str):
        for key in self.__accounts:
            if key == name:
                self.__accounts[key] += value
                with open("/mnt/logs.txt", "a", encoding="utf-8") as logs:
                    print(f"{datetime.datetime.now(datetime.timezone(datetime.timedelta(hours=3))).strftime('%Y-%m-%d %H:%M')};{name};{value};{description}", file=logs)
                self.save()


bank = Bank()


@app.route("/")
def index():
    return render_template("index.html", accounts=bank.accounts)


@app.route("/admin/", methods=("GET", "POST"))
def admin():
    status = ""
    if request.method == "POST":
        form = request.form.to_dict(flat=False)
        if check_code(request.form["code"]):
            for name in form["name"]:
                bank.add_transaction(name, int(form["value"][0]), form["description"][0])
            status = "success"
        else:
            status = "error"
    return render_template("admin.html", accounts=bank.accounts, status=status)


@app.route("/history/")
def history():
    with open("/mnt/logs.txt", "r", encoding="utf-8") as logs:
        history_str = [log.split(";") for log in logs.read().split("\n")[::-1] if log != ""]
    children = list(bank.accounts.keys())
    goal = request.args.get("name")
    if goal:
        history_str = [row for row in history_str if goal == row[1]]
    return render_template("history.html", history=history_str, children=children)


@app.route("/shop/")
def shop():
    with open("/mnt/shop.json", "r") as file:
        items = json.load(file)
    return render_template("shop.html", items=items)
